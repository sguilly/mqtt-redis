/**
 * Created by sguilly on 24/11/14.
 */

var bunyan = require('bunyan');

var mqtt = require('mqtt');
var redis = require("redis");

var _ = require('underscore');

var EventEmitter = require('events').EventEmitter,
  util = require('util');

var clientRedis;

function Consumer(opts) {
  if (!(this instanceof Consumer)) {
    return new Consumer(opts);
  }
  this.options = opts;
  this.transformKeys = Object.getOwnPropertyNames(this.options.decoder.transform);
  this.log = bunyan.createLogger(this.options.logger);


  this.log.trace('Application started');

  EventEmitter.call(this);
}

util.inherits(Consumer, EventEmitter);

function zrange(key)
{
  clientRedis.zrange(key, 0, -1, function (err, res) {

    console.log(key+'='+res);

  });
}

Consumer.prototype.store = function(obj) {

  //console.log(obj);

  console.log('store in redis');

  clientRedis.hmset(obj['id'],obj);
  //clientRedis.hmset(obj['id'],'avg','3');

  for (var propertyName in obj) {

    //clientRedis.del(obj['id'] + ':' + propertyName);

    if(propertyName === 'id' || propertyName === 'time') continue;


    clientRedis.zadd(obj['id'] + ':' + propertyName, obj['time'].getTime(), obj['time'].getTime() + ':' + obj[propertyName], function (err, response) {
      if (err) throw err;
    });

    //zrange(obj['id'] + ':' + propertyName);

  }


  clientRedis.hgetall(obj['id'], function (err, res) {
    console.log('read redis again : hgetall');
    console.log(res);
  });


};

Consumer.prototype.open = function () {
  var that = this;

  var clientMqtt = mqtt.createClient(this.options.mqtt.port, this.options.mqtt.ip, {
    clean: false,
    encoding: 'utf8',
    clientId: this.options.mqtt.clientId
  });

  clientRedis = redis.createClient(this.options.redis.port,this.options.redis.host,{});

  clientMqtt.subscribe(this.options.mqtt.subscribe, {qos: this.options.mqtt.qos});

  clientMqtt.on('message', function (topic, message) {
    that.log.trace(topic);
    that.log.trace(message);

    var obj = JSON.parse(message);

    var point = {};

    var indexId = -1;
    var indexTime = -1;

    for (var propertyName in obj) {


      if(that.options.decoder.denyKeys.indexOf(propertyName) > -1)
      {
        continue;
      }

      if(indexId === -1)
      {
        indexId = that.options.decoder.idKeys.indexOf(propertyName);
        if(indexId > -1)
        {
          point['id'] = obj[propertyName];
          continue;
        }
      }

      if(indexTime === -1)
      {
        indexTime = that.options.decoder.timeKeys.indexOf(propertyName);
        if(indexTime > -1)
        {
          var dateObj = new Date(obj[propertyName]);
          point['time'] = dateObj;
          continue;
        }
      }

      var value = obj[propertyName];

      var idTransform = that.transformKeys.indexOf(propertyName);

      if(idTransform > -1)
      {
        propertyName = that.options.decoder.transform[that.transformKeys[idTransform]];
      }

      if(that.options.decoder.allowString || _.isNumber(value) )
      {
        point[propertyName] = value;
        continue;
      }

    }

    if(indexTime === -1)
    {
      point['time'] = new Date();
    }

    that.store(point);


  });
};

module.exports = Consumer;
