/**
 * Created by sguilly on 24/11/14.
 */

var PrettyStream = require('bunyan-prettystream');

var prettyStdOut = new PrettyStream();
prettyStdOut.pipe(process.stdout);

//var mqttRedis = require('../lib/mqtt-redis');  //FROM THE
var mqttRedis = require('mqtt-redis');


var opts = {
  logger: {
    name: 'mqtt-redis-example',
    streams: [{
      level: 'trace',
      type: 'raw',
      stream: prettyStdOut
    }]
  },
  mqtt: {
    ip : '188.213.25.148',
    port: 3001, // tcp
    topic: 'mqtt-zibase/#',
    clientId : 'mqtt-redis',
    subscribe : ['mqtt-teleinfo/#','mqtt-zibase/#'],
    qos : 1 // 0 : without persistence and no ACK | 1 : with offline mode and ACK
  },
  redis :{
    host : '188.213.25.148',
    port : 6379, // optional, default 8086
    password : 'root'
  },
  decoder :{
    idKeys : ['id'],
    timeKeys : ['time','date'],
    denyKeys : ['power'],
    transform : {tem : 'temperature', hum : 'humidity'},
    allowString : false
  }
};

var consumer = mqttRedis(opts);

consumer.open();
